<?php 

namespace Fifteen\HtmlElement;

class VoidHtmlElement extends HtmlElement
{

    public function __construct($tagName, $attributes = [])
    {
        parent::__construct($tagName, null, $attributes, true);
    }

}