<?php 

namespace Fifteen\HtmlElement;

use Fifteen\HtmlElement\VoidElement;

class HtmlElement
{
    protected $attributes = [];
    protected $tagName;
    protected $content = '';
    protected $voidElement = false;
    protected $closeVoidElements = true;    // close tags like <br /> if true, <br> if false 
    protected static $voidElements = ['area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img', 'input', 
                                                'keygen', 'link', 'meta', 'param', 'source', 'track', 'wbr'];

    public function __construct($tagName, $content = null, $attributes = [], $voidElement = false)
    {
        $this->setTagName($tagName);
        $this->setContent($content);
        $this->setAttributes($attributes);
        $this->voidElement($voidElement);
        // TODO: Set closing void elements from config options?)
    }

    public static function create($tagName, $content, $attributes = [], $voidElement = false)
    {
        return new HtmlElement($tagName, $content, $attributes, $voidElement);
    }

    public static function createVoid($tagName, $attributes = [])
    {
        return new VoidHtmlElement($tagName, $attributes);
    }

    public function render()
    {
        $attributes = $this->buildAttributes();
        $content = $this->buildContent();
        if ($this->voidElement) {
            $output = '<' . $this->tagName . $attributes . ($this->closeVoidElements ? ' />' : '>');
        } else {
            $output = '<' . $this->tagName . $attributes . '>' . $content . '</' . $this->tagName . '>';
        }
        return $output;
    }

    protected function buildAttributes()
    {
        $output = '';
        foreach ($this->attributes as $key => $value) {
            $output .= ' ' . $this->buildAttributeElement($key, $value);
        }
        return $output;
    }
    
    protected function buildAttributeElement($key, $value)
    {
        // For numeric keys we will assume that the key and the value are the same
        // as this will convert HTML attributes such as "required" to a correct
        // form like required="required" instead of using incorrect numerics.
        if (is_numeric($key)) {
            $key = $value;
        }
        if ( ! is_null($value)) {
            return $key . '="' . e($value) . '"';
        }
    }

    protected function buildContent()
    {
        if ($this->content instanceof HtmlElement) {
            return $this->content->render();
        } else {
            return $this->content;
        }
    }

    public function addAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
    }


    /**
    *   Setter methods:
    **/

    public function setAttributes($item)
    {
        $this->attributes = $item;
    }

    public function setTagName($item)
    {
        $this->tagName = $item;
    }

    public function setContent($item)
    {
        $this->content = $item;
    }

    public function voidElement($item)
    {
        $this->voidElement = !empty($item);
    }

    public function closeVoidElements($item)
    {
        $this->closeVoidElements = !empty($item);
    }

    /**
    *   Getter methods:
    **/

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function getTagName()
    {
        return $this->tagName;
    }

    public function getContent()
    {
        return $this->content;
    }

    public static function isVoidElement($tagName) 
    {
        return in_array($tagName, self::$voidElements);
    }

    public static function __callStatic($name, $arguments)
    {
        $arg_1 = isset($arguments[0]) ? $arguments[0] : null; 
        $arg_2 = isset($arguments[1]) ? $arguments[1] : [];
        $arg_3 = isset($arguments[2]) ?  $arguments[2] : false;
        if (static::isVoidElement($name)) {
            return static::createVoid($name, empty($arg_1) ? [] : $arg_1);
        } else {
            return static::create($name, $arg_1, $arg_2, $arg_3);
        }
        
    }

    public function __toString()
    {
        return $this->render();
    }

}