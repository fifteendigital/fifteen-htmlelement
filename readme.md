# Html Element

A really simple package that helps manage and render HTML elements. 

###Installation

Add HtmlElement to your composer.json file:

    "require": {
        "php": ">=5.5.9",
        "laravel/framework": "5.1.*",
        "fifteen/htmlelement": "1.0.*"
    },

Add minimum-stability: dev:
```sh
    "minimum-stability": "dev",
```

As the repository isn't on Packagist, you also need to include it in the repositories list:

```php
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:fifteendigital/fifteen-htmlelement.git"
        }
    ]
```

Run composer update:

```sh
> composer update
```

### Usage

```php
use Fifteen\HtmlElement\HtmlElement;

$paragraph = HtmlElement::create('p', 'Paragraph content');
echo $paragraph->render();

$break = HtmlElement::createVoid('br');
echo $break->render();

echo HtmlElement::hr();
```